package com.auth.controller;

import javax.annotation.security.RolesAllowed;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/key")
public class keycloakController {
	
	@GetMapping("/dummy")
	@RolesAllowed("admin")
	String getDummy() {
		return "admin";
	}
	
	@GetMapping("/dummy1")
	@RolesAllowed("user")
	String getDummy1() {
		return "user";
	}

}
